<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Shop extends Model
{
    //
    public static function getRules()
    {
        return [
            'regionId' => 'required|integer',
            'title' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'userId' => 'required|integer',

        ];
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate()
    {
        $validator = Validator::make($this->get(), self::getRules());

        if (!empty($validator->errors()->all())) {
            return false;
        } else {
            return true;
        }

    }

    public function set($data)
    {
        foreach ($data as $column => $value) {
            $this->$column = $value;
        }

    }

    public function get()
    {
        return $this->toArray();

    }


}
