<?php

namespace App\Console\Commands;

use App\Shop;
use Illuminate\Console\Command;
use League\Csv\Reader;

/**
 * Class ImportCsv
 * @package App\Console\Commands
 *
 * Тестовое задание, импорт из CSV в базу данных
 */
class ImportCsv extends Command
{


    /**
     * Имя консольной команды
     *
     * @var string
     */
    protected $signature = 'command:import';

    /**
     * Описание консольной команды
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Создание инстанса
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     *
     * Имя файла
     * @return string
     *
     */
    private static function getFileName()
    {
        return storage_path('shops/shops');


    }

    /**
     * Читает csv файл в массив
     * @return array
     */
    private function readFileToArr()
    {
        $file = $this->getFileName();

        $csv = Reader::createFromPath($file, 'r');

        $rows = $csv->fetchAll();

        return $rows;

    }

    /**
     * Чтение файла и запись в базу
     *
     * @return mixed
     */
    public function handle()
    {


        $rows = $this->readFileToArr();

        $report = ['success' => 0, 'error' => 0];
        foreach ($rows as $key => $value) {

            /**
             * не записывать заголовки(пропуск первой итерации)
             */
            if ($value === reset($rows)) {
                continue;
            }

            /**
             * Формирование массива с данными из строки
             */
            $modelArrData = [
                'regionId' => (int)$value[0],
                'title' => $value[1],
                'city' => $value[2],
                'address' => $value[3],
                'userId' => (int)$value[4],
            ];


            /**
             * проверка данных и запись
             */
            $model = new Shop();
            $model->set($modelArrData);

            $save = null;
            if ($model->validate()) {
                $save = $model->save();
            }


            /**
             * Логирование отчета
             */
            if ($save) {
                $report['success']++;
                echo 'inserted row';
            } else {
                $report['error']++;
                echo '!!err!!';
            }

            echo PHP_EOL;

        }


        /**
         * Вывод отчета
         */
        echo "-------------------------------------------\n";
        echo "Inserted " . $report['success'] . "\n";
        echo "With errors " . $report['error'] . "\n";


    }
}
