*Тестовое задание исполнено на Laravel Framework 5.4.36*

*Работа велась в 2х файлах, это класс Консольного скрипта и класс Модели SHOP*

1. app/Console/Commands/ImportCsv.php
2. app/Shop.php

*Так же добавлен файл миграций который создает таблицу Shops*




 
 
 
 #Установка
 
 *клон репозитория*
 
 `git clone git@bitbucket.org:gennadiy_bilyk/hismith.git .`

*Обновление пакетов*

`composer update`

#Настройка

*Необходимо указать настройки базы в файле .env*

`DB_CONNECTION=mysql`

 `DB_HOST=127.0.0.1`

 `DB_PORT=3306`
 
 `DB_DATABASE=`
 
 `DB_USERNAME=`
 
 `DB_PASSWORD=`
 
 *Накатить миграции*
 
 `php artisan migrate`

##Запуск скрипта
`php artisan command:import`